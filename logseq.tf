resource "docker_image" "logseq-webapp-img" {
  name = "ghcr.io/logseq/logseq-webapp:latest"
}

resource "docker_container" "logseq_container" {
  image = docker_image.logseq-webapp-img.image_id
  name  = "logseq"
  env = [
    "TZ=America/New_York"
  ]
  ports {
    ip       = "127.0.0.1"
    internal = 80
    external = 3001
  }
}