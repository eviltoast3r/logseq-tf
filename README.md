# Logseq Terraform
This is a terraform module used to run a webapp version of [Logseq](https://logseq.com/) as a docker container on your local machine

At some point I'll adapt this for a K8s cluster

Unfortunately it uses your local storage anyway so it's not much different from just installing the app....

> Converted from a docker command found [here](https://github.com/logseq/logseq/blob/master/docs/docker-web-app-guide.md)

## Usage
* clone this repo
* terraform init, plan, apply
* Logseq will be available on https://localhost:3001